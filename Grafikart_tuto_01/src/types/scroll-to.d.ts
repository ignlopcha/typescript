declare module "scroll" {//elle est dans un module, C'EST QUOI UN MODULE??
    const scrollTo: (x: number, y: number, options: {
        ease?: string, //permet definir la function d'animation
        duration?: number // permet definir la duration de l'animation
    }) => void

    export default scrollTo  //comme je suis dans un module il faut definir comment l'exporter
}