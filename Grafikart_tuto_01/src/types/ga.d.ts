//Option 1
// on declare variables accesibles globalement
/* declare var ga: (eventName: string, options: { // on se base sur la signature original de google analitics
    hitType: string,
    eventCategory?: string //? pour dire que c'est optionel
}) => void; */

//Option 2 modification interface Window
//Les interfaces restent ouvertes a la modification
//Advantage???
interface Window {
    ga: (eventName: string, options: { // on se base sur la signature original de google analitics
        hitType: string,
        eventCategory?: string //? pour dire que c'est optionel
    }) => void;
}
