/* const a: String = "hello world"; //type String
const b = "hello world"; //type String literal "hello world"

//declaration de un objet
const user: { firstname: string, lastname?: string } = { firstname: "John", lastname: "Doe" }
//avec le ? on veut dire paramete optionelle
const user1: { firstname: string, [key: string]: string } = { firstname: "John", lastname: "Doe" }
//on met comme deuxieme parametre un paire cle valeur comme ça on peut creer un pattern dinamique

const cb: (e: MouseEvent) => void = (e: MouseEvent): void => {

}
function printId  (id: number | string): void { //accepte un number ou un string
    console.log(id.toString());
} */

const compteur = document.querySelector("#compteur") as HTMLButtonElement; //pour "caster la sortie"
let i = 0;

const increment = (e: Event) => {
    e.preventDefault();
    i++
    const span = compteur?.querySelector("span")//il compile pas si compteur peut etre null
    if (span) {
        span.innerText = i.toString()
    }
}
//pour faire qu'il compile meme si c'est possible que compteur soit null
compteur?.addEventListener("click", increment);

compteur.addEventListener("click", increment)

//-----------Alias & Generics -------------------------
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//Alias
type User = { firstname: string, lastname: string };

const user: User = { firstname: "John", lastname: "Doe" }

//Generique

function identity(arg: any): any {
    return arg;
}

const aa = identity(3); //aa perds son type et deviens type any

console.log(aa);

//pour l'eviter

function identity2<ArgType>(arg: ArgType): ArgType {
    return arg;
}

const bb = identity2<number>(3);//type number 
const cc = identity2(3);//type string literal 3

/* function first<Type>(arg: Type[]): Type {
    return arg[0]
} */

function first<Type>(arg: Type[]): Type {
    return arg[0]
}
const dd = first(["asf", "asfaf", "asfg"]) // dd type string

const ee = ["afsfa", "gaege", 3]; //type string ou number

const ff: Array<string | number> = ["afsfa", "gaege", 3]; //type string ou number

type Identity<ArgType> = (arg: ArgType) => ArgType;

////////////////////////////////////
//Apliquer contraintes
function consoleSize<Type>(arg: Type): Type {
    console.log(arg.length) //ça marche pas parce qeu sur abb on peut pas apliquer .lenght
    return arg
}

const abb = consoleSize(3);

function consoleSize2<Type extends { length: number }>(arg: Type): Type { //contraintre: extends un objet qui a une cle type lenght
    console.log(arg.length) //ça marche pas parce qeu sur abb on peut pas apliquer .lenght
    return arg
}

const abc = consoleSize2(3); //l'erreur s'affiche dans l'element puis que number n'a pas une cle type lenght

const bac = consoleSize2([3, 2]); // il accepte un Array extends d'un objet qui a une cle type lenght

/////////////////////////
type P = keyof User;

type Username = User["firstname"]; // type string


const user1 = {
    firstname: "John",
    lastname: "Doe",
    age: 32
}

type User1 = typeof user //User 1 prends le type de user1


//-----------Classes-------------------------
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//Propiete readonly

function reverse<T>(arr: readonly T[]): T[] { // readonly evite que le logiciel puisse faire des modifs
    return [...arr].reverse();   // la methode reverse() modifie le tableau original
    //spread operator ... pour creer un nouveau tableau puis que on peut pas modifier l'original https://www.youtube.com/watch?v=pYI-UuZVtHI
}

//Classes
///////////////////////////////////////////////////////
//Private, protected public
//Se tien juste en compte pour le typescript pas dans le code js produit
//Si vraiment on veut prive dans le code js il faut ajouter #
class A {
    private a = 3;
    protected b = 3; // les enfants peuvent acceder a cette propieté
    public c = 3; //accesible par tous
    #d = 3; //dans le code js utilise un sisteme de mapping pour le rendre privé

    log() {
        console.log(this.a);
    }

    logD() {
        console.log(this.#d);
    }

}

const aInstance = new A();

console.log(aInstance.a); //On peut pas acceder a en dehors de la classe

aInstance.log(); //valide puisque le methode log() n'est pas privé

class B extends A {
    log() {
        console.log(this.b); //possible parce que b dans class A est protected
    }
}

console.log(aInstance.c); // possible parce que c est public

//Constructor 

class C {
    constructor(
        public a: number) {

    }
}

//Generiques dans les classes

class Collection<T> {
    constructor(private items: T[]) {

    }

    first(): T | null {
        return this.items[0] || null //retourne le premier element du tableau si ça n'existe pas renvoie null
    }
}

const a = new Collection<number>([1, 2]); //specification explicite de type number
const f = a.first(); // est type number or null

const g = new Collection([1, 2]); // sans specification explicite
const h = g.first;                // est type number or null il le sait par le constructeur

//Modification de this

class Subscriber {
    on(this: HTMLInputElement, name: string, cb: Function) { //this va faire reference a un element HTMLInputElement et pas a l'instantiation de la classe
        this.addEventListener                                                        //this. sors les methodes de HTMLInputElement
    }
}

//Comparation des classes

class Point {
    x = 0;
    y = 0;
}

class Geometry {
    x = 0;
    y = 0;
    surface = 0;
}

function getX(p: Point) { //prends un objet type Point comme paramentre
    return p.x
}

getX(new Geometry()); // il verifie pas que c'es une instance de quelque chose il regarde juste la forme, donc ça passe

// Abstract Classes

abstract class Geometry2 {
    x = 0
    y = 0
    abstract surface(): number //methode surface abstract
}

class Triangle extends Geometry2 { //vu qu'elle extends Geomtry2 est oblige a implementer toutes ses methodes abstraites
    x = 2;
    y = 2;
    surface() {
        return 3
    }
}

//Static
abstract class Geometry3 {
    static origin = { x: 0, y: 0 }
}

Geometry3.origin; //on peut acceder de dehors parce que origin est static

//-----------Interfaces-------------------------
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//Diffrences entre type et interface  https://www.youtube.com/watch?time_continue=431&v=sFNQeh5Oc08&feature=emb_title

//-----------Tuple & Enum-------------------------
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//Unknown
//similaire a any il ne peut pas etre utilise avant d'etre precisé
//preferable a utuliser
function j(arg: unknown) { //il faut especifier le type 
    if (arg instanceof HTMLInputElement) { //on utilise le narrowing
        arg.value = "hello";
    }
}

//Tuple
//permet s de faire la differnece entre un tableau qui a une taille fixe et une qui a une taille variable
const k = [1, 2, 3,]; //type number[]
const l = [1, 2, 3] as const // type readonly [1,2,3], as const => ça va pas changer

type ListItem = [string, number] // il accepte SEULEMENT un string et un number, dans cet ordre


const m: ListItem = ["tomate", 2]; // il accepte SEULEMENT un string et un number, dans cet ordre
const n: (string | number)[] = ["tomate", 2] // il accepte un string OU un number, sans importer le nombre

const o: ListItem = ["banane", 3];

function merge<T extends unknown[], U extends unknown[]>(a: T, b: U): [...T, ...U] {//prends de choses generiques T et U, en parametre a et b, retourne un TUPLE qui contiendra les valeurs dans a et les valeurs dans b
    return [...a, ...b];


}

const r = merge(m, o); // r type [string, number, string, number]


//Enum

enum STEPS {
    Intro = "Intro", //par defaut valeur  cle 0 // modification a cle "Intro", plus facile pour debugger
    Selection = "Selection", //par defaut valeur  cle 1
    Panier = "Panier", //par defaut valeur  cle 2
    Paiement = "Paiement" //par defaut valeur  cle 3
}

enum STEPS2 {
    Intro,
    Selection,
    Panier,
    Paiement
}


const step: STEPS = STEPS.Selection //par defaut on la met en selection
console.log(step); // => par defaut 1, apres modification "Selection"

const step2: STEPS2 = STEPS2.Selection

console.log(STEPS2[step2])//reverse mappig permet ovtenir le nom a partir de l'index  => par defaut 1, apres modification "Selection"


//Enumerateur de type constant

const enum STEPS3 {
    Intro = "Intro", //par defaut valeur  cle 0 // modification a cle "Intro", plus facile pour debugger
    Selection = "Selection", //par defaut valeur  cle 1
    Panier = "Panier", //par defaut valeur  cle 2
    Paiement = "Paiement" //par defaut valeur  cle 3
}

//-----------Fichiers de declaration-------------------------
//Pour utiliser des librairies utilises que ne sont pas types
//Sur des librairies modernes on peut trouver les fichiers de declaration deja faits
//////////////////////////////////////////////////////

export class Point1 {
    x = 0;
    y = 0;

    move(x: number, y: number) { //methode pour faire bouger le point
        this.x += x;
        this.y += y;
        return this; // pour pouvoir enchainer le methode
    }
}

//Option 1
/* ga("send", {  //par defaut TypeScript ne connais pas la variable ga, il faut creer un fichier de definition
    hitType: "event",
    eventCategory: "category"
}) */

//Option 2
window.ga("send", {  //par defaut TypeScript ne connais pas la variable ga, il faut creer un fichier de definition
    hitType: "event",
    eventCategory: "category"
})

//on installe une libraire qui n'a pas de fichier de declaration, EXEMPLE https://www.npmjs.com/package/scroll-to
//on va devoir creer notre fichier de declaration associé
//pour installer la libraire: npm install scroll-to

//on copy-paste l'API et on modif
//a la base 
/* var scrollTo = require('scroll-to');
scrollTo(500, 1200, {
    ease: 'out-bounce',
    duration: 1500
}); */

import scrollTo from "scroll-to" //il est pas capable de trouver le module nod..js il faut modifier la tsconfig "moduleResoluton"

scrollTo(500, 1200, {
    ease: 'out-bounce',
    duration: 1500
});

//-----------Type personnalisé------------------------
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//Creation de types a partir de un objet qui esxiste deja
//////////////////////////////////////////////////////
class Poisson {

}

class Chat {

}

function generator(options: { nager: any } | { sauter: any }) {  //soit genere un nouveau poisson, soit un nouveau chat. Ça depend si il a l'option nager ou sauter
    if ("nager" in options) {
        return new Poisson();
    } else {
        return new Chat();
    }
}

const p = generator({ nager: "ace" }); // p est type Poisson ou Chat, je vuex qu'il reconnaise le type Poisson =>Types conditionnelles

//Types conditionelles
function generator2<T extends { nager: any } | { sauter: any }>(options: T): T extends { nager: any } ? Poisson :   //Ils s'ecrivent comme un turner
    Chat {
    if ("nager" in options) {
        return new Poisson();
    } else {
        return new Chat();
    }
}

const q = generator2({ nager: "ace" }); //capable de reconnaitre le type Poisson

//Simplification 
type AnimalOption = { nager: any } | { sauter: any };
type AnimalFromOption<T> = T extends { nager: any } ? Poisson : Chat;

function generator3<T extends AnimalOption>(options: T): AnimalFromOption<T> {
    if ("nager" in options) {
        return new Poisson();
    } else {
        return new Chat();
    }
}

//Extraire le type selon le resultat de la methode cri

class Poisson2 {
    cri() {
        return false;
    }

}

class Chat2 {
    cri() {
        return "miaiu";
    }
}

type AnimalCri<T> = T extends { cri: () => infer U } ? U : never;

type r = AnimalCri<Chat2>; //r type string

//Mapped Type
//////////////////////////////////////////////////////

class FeatureFlags {
    env = "hello";
    darkMode() { return true };
    privateMode() { return true };
    nfswMode() { return true };
}
//je veux crer un type de confiuration qui contiens que de booleans et le cle correspond a le nom de chaque methode

type OptionsFlag<T> = {
    +readonly [key in keyof T]+?: T[key] extends () => boolean ? boolean : never //+? ajoute optionel pour toutes les propietes, +readonly toutes les propietes sont en readonly
}

type s = OptionsFlag<FeatureFlags>;

type OptionsFlag2<T> = {
    +readonly [key in keyof T as `get${string & key}`]: T[key] extends () => boolean ? boolean : never; //+? ajoute optionel pour toutes les propietes, +readonly toutes les propietes sont en readonly
}

type s2 = OptionsFlag2<FeatureFlags>;

//-----------Decorators------------------------
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

function CustomElement(name: string) { //Creation de function avec le meme nom de le decorateur
    return function (constructor: typeof HTMLElement) { //Retourne une function qui prends en parametre le constructeur, s'execute de le lancement de notre script
        customElements.define(name, constructor);

    }

}

@CustomElement("demo-hello") // Definir le CustomElement vers un decorator, il faut l'ajouter dans le fichier HTML
class Demo extends HTMLElement { //extends pour creer un CustomElement
    connectedCallback() {
        this.innerHTML = "Hello World";
    }
}

//Validation d'une propriete

function Constraint({ min, max }: { min: number, max: number }) {
    return function <T>(target: T, key: keyof T) { //si pqr exemple on met key "age", on pourra utiliser le contraintre que sur l'age
        let val = target[key] as any;

        const setter = (v: unknown) => {
            if (typeof v == "number" &&
                v > min &&   //ajout de contraintres sur le age min et max
                v < max

            ) {
                val = v
                return
            }
            throw new Error(`On attend un nombre entre ${min} et ${max}`);
        }

        const getter = () => val; //GETTER - une function aue retorune juste la valeur

        Object.defineProperty(target, key, {  //Modification de Objet; target c'est l'instance de notre objet , key c'est la propiete a modifier
            set: setter,
            get: getter
        })

    }

}


class User {

    @Constraint({
        min: 0,
        max: 100
    })
    age: number = 0
}

const u = new User();
u.age = 20; // doit reenvoyer un erreur si n'est pas un number, ou si n'est pas entre le min et le max
console.log(u.age);