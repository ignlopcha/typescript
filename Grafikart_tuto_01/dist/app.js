"use strict";
//-----------Decorators------------------------
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
function CustomElement(name) {
    return function (constructor) {
        customElements.define(name, constructor);
    };
}
let Demo = class Demo extends HTMLElement {
    connectedCallback() {
        this.innerHTML = "Hello World";
    }
};
Demo = __decorate([
    CustomElement("demo-hello") // Definir le CustomElement vers un decorator, il faut l'ajouter dans le fichier HTML
], Demo);
//Validation d'une propriete
function Constraint({ min, max }) {
    return function (target, key) {
        let val = target[key];
        const setter = (v) => {
            if (typeof v == "number" &&
                v > min && //ajout de contraintres sur le age min et max
                v < max) {
                val = v;
                return;
            }
            throw new Error(`On attend un nombre entre ${min} et ${max}`);
        };
        const getter = () => val; //GETTER - une function aue retorune juste la valeur
        Object.defineProperty(target, key, {
            set: setter,
            get: getter
        });
    };
}
class User {
    constructor() {
        this.age = 0;
    }
}
__decorate([
    Constraint({
        min: 0,
        max: 100
    })
], User.prototype, "age", void 0);
const u = new User();
u.age = 20; // doit reenvoyer un erreur si n'est pas un number, ou si n'est pas entre le min et le max
console.log(u.age);
