declare function CustomElement(name: string): (constructor: typeof HTMLElement) => void;
declare class Demo extends HTMLElement {
    connectedCallback(): void;
}
declare function Constraint({ min, max }: {
    min: number;
    max: number;
}): <T>(target: T, key: keyof T) => void;
declare class User {
    age: number;
}
declare const u: User;
